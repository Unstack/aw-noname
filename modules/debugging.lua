return {
  setup={
    pre=function(aw,some)
          print(type(some))
          some.benchmarks["setup"] = aw.mark()
        end,
	post=function(aw,some)
          some.benchmarks["setup"] = aw.mark(some.benchmarks["setup"])
        end},
  start=
  {
	pre=function(aw,some)
	    some.benchmarks["start"] 	= aw.mark(some.benchmarks["start"])
	end,
	post=function(aw,some)
	    some.benchmarks["init"] 	= aw.mark(some.benchmarks["init"])
	    some.benchmarks["start"] 	= aw.mark(some.benchmarks["start"])

	    aw.print("[Debug information]")
	    aw.print("[ Benchmarks ]")

	    for k,v in pairs(some.benchmarks) do
	      aw.print(k..":",math.floor(v*1000).."ms")
	    end
	end
  },
  init=function(aw,some)
    some.benchmarks = {init=aw.mark()}
  end
}
