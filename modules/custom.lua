return {
  init=function(aw,some,config)
  --[[
    All your unsafe, pagan code may peacefully coexist among the other
    modules by implementing it as signal callbacks.

    Two signals exist for this: awe.setup and awe.start.
    Anything that you need in awe.start you can create in awe.setup.

    If you need a container to store things (across modules or events)
    put it in 'some'.

    Don't try anything funny outside of these callbacks unless you know what
    you're doing. You might make aw cry.
  --]]
  aw:connect_signal("aw::setup",function()
    aw.beautiful.font = "Verdana 12"
  end)

end}
