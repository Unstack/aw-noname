-- Your configuration options
local myconfig = {
  debug=true,
}

-- Setup awesome and pass configuration
-- Any modules will be loaded by default
--(Unless you specify a whitelist)
require("./aw")(myconfig)
