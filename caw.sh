#!/usr/bin/env bash


CURRENT_CONFIG=$()
declare -A CAW

usage(){
  echo "Control aw - remote control & configuration utility for aw"
}

caw_config(){
  if [ -z $2 ];
    then
  echo $CURRENT_CONFIG
  fi
}

CAW['config']=caw_config

if [ -z $1 ];
  then
    usage
    exit
fi

if  [ ${CAW[$1]} ];
then
  echo $(${CAW[$1]})
else
  echo "Unknown command:" $1
fi
