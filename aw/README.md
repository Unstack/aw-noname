# awm-noname

90K Micro-framework for awesome featuring modular loading, reloading, events, error handling and more.

## Installation
```sh
cd ~/.config/awesome
mv rc.lua rc.lua.bak
git clone git@gitlab.com:Unstack/awm-noname.git .
```

## Usage

### Switching from vanilla
If you already have a heavily customized Awesome setup but would like to switch over to aw, you can do so by placing your modified `rc.lua` (and any other required files) inside the provided `awesome/modules` directory, and they will be ran alongside aw's core modules. If you would like to disable the core modules provided, set the `disableCore = true` in your new `rc.lua`. This might provide helpful whilst transferring your scripts to aw, as you will likely encounter a bunch of collisions.

### aw principles
At it's core aw is a utility to keeps all the awesome libraries neatly tucked under a single variable. At it's best it can be a dynamic module loader that may self-reports collisions ahead of time. To comply with the Awesome framework (and any future updates) aw is designed to be open-ended and flexible as possible. The desktop environment aw provides through its core modules is therefor identical to the one provided by the default awesome configuration. Any user modifications can now be performed by adding additional modules to the ones provided by aw.

aw provides you with two callbacks (aw::setup,aw::start) to execute your configuration through. Both of these callbacks provide the following arguments to help keep your Awesome scripts neat and modular:

#### aw
All Awesome libraries are stored in this table. The only exception being awful which has been renamed to `aw.ful`.

### some
aw intends to keeps most functionality and state in a separate container named `some`, which can be accessed across modules. You are free to use this container for pretty much anything though, use it as your new global object if you wish. As a bonus, `some` also doubles as a framework-wide signal emitter for your own purposes.

#### config
Anything you pass to `aw.setup` will be available to you inside your modules through the `config` variable. Feel free to build your own archaic system of configuration options to make your modules as customisable as possible. A set of options for the core-modules is provided in `aw/defaultConfig.lua`. These will be used in the case they are not provided by the user during `aw.setup`.

## Core modules
|Name|description|
--|--
|clientkeys|Predefined set of hotkeys for clients (application windows)
|globalkeys|Predefined set of keyboard hotkeys
|handle_errors|Basic error handler
|helpers|Helpers for aw
|layouts|Table of awful layouts
|mouse|Predefined set of mouse button hotkeys
rules|Predefined set of screen rules
|screen|Default screen setup
|setup|Desktop booter for awesome
|sidemenu|Helpful menu with basic awesome utilities
|signals|Signals for clients
|taskbar|A taskbar wibar for awesome

## some signals
*In order of execution:*
|Name|description|
--|--
setup| Anything in `setup` will always be ran before anything else.
start| Executed when aw runs, this is where your apply the things you've setup before.


## Comparing vanilla to aw
*Your average setup.rc:*
```lua
-- Changes the default font to 'Verdana 12'
aw.beautiful.font = "Verdana 12"
```
*Your setup.rc done awlike:*
```lua
-- aw expects every module to return a table with
-- at least an init call
return {  
  -- The init call is ran when aw starts.
  -- The provided `aw` parameter is a table
  -- of the default Awesome libraries.
  init=function(aw)
    -- Changes the default font to 'Verdana 12'
    aw.beautiful.font = "Verdana 12"
end}
```

_Your setup.rc done awlike and eventfully:_

```lua
return {  
  -- Along with `aw` a container object is provided
  -- as well as the configuration settings in use.
  init=function(aw,some,config)
    local myfont

    -- Connect a function to aw's setup signal.
    -- setup calls will happen before anything else
    some:connect_signal("aw::setup", function()
      -- Apply myfont unless it was set by another module
      myfont = some.font or config.font
    end)

    -- This will be run when aw starts, straight after setup.
    some:connect_signal("aw::start", function()
    -- Changes the default font to 'Verdana 12'
      aw.beautiful.font = myfont
    end)
  end
}
```


## Core configuration options
```lua
terminal = "xfce4-terminal", -- This is used later as the default terminal and editor to run
editor = "mousepad",
editor_cmd = "mousepad",
-- Default modkey.
-- Usually, Mod4 is the key with a logoLastE between Control and Alt.
-- If you do not like this or do not have such a key,
-- I suggest you to remap Mod4 to another key using xmodmap or other tools.
-- However, you can use another modifier like Mod1, but it may interact with others.
modkey = "Mod4",
theme = "zenburn/theme.lua", -- Global theme
font = "Ubuntu 12",          -- Global Font
taskbar = {
  position = "bottom",
  height = 30,
  content = {}
},
workspaces = {
  "www","work","email","media","extra"
},
desktop = {
  --Fallback wallpaper (only displayed if workspace doesn't have one)
  wallpaper="/home/user/mywallpaper.jpeg"
}
}
```
