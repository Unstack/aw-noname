
return {about = {
  blurb="Menu with Awesome utilities",
  description=[[
    Creates a menu with useful shortcuts in some.menu.
  ]],
  requires={
    aw={"ful","gears","hotkeys"}
  },
  overrides={
    some={"menu"}
  },
},
init=function(aw,some,config)

  some.menu = aw.ful.menu({
                              items = {
       { "open terminal", config.terminal },
       { "hotkeys", function() return aw.hotkeys.show_help() end},
       { "manual", config.terminal .. " -e 'man awesome' " },
       { "edit config", config.editor_cmd .. " " .. awesome.conffile },
       { "restart", awesome.restart },
       {"xterm","xterm"},
       { "quit", function() awesome.quit() end}
     },
     height = 200,
     width = 150
    })
  end
}
