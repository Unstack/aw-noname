return {
  about = {
    blurb="Predefined set of mouse button hotkeys",
    description=[[
      Sets some.mouse to a lua table of awful buttons
    ]],
    requires={
      aw={"ful","gears"},
    },
    overrides={
      some={"mouse"}
    },
  },init=function(aw,some)
   some.mouse = aw.gears.table.join(
    aw.ful.button({ }, 1, function () some.menu:hide()   end),
    aw.ful.button({ }, 3, function () some.menu:toggle() end),
    aw.ful.button({ }, 4, aw.ful.tag.viewnext),
    aw.ful.button({ }, 5, aw.ful.tag.viewprev)
  )
end}
