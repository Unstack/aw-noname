return {about = {
  blurb="Table of awful layouts",
  description=[[
    Sets some.layouts to ta table of awful layouts.
  ]],
  requires={
    aw={"ful"},
  },
  overrides={
    some={"layouts"}
  }
},
  init=function(aw,some)
    -- Table of layouts to cover with awe.ful.layout.inc, order matters.
  some.layouts = {
      aw.ful.layout.suit.floating,
      aw.ful.layout.suit.tile,
      aw.ful.layout.suit.tile.left,
      aw.ful.layout.suit.tile.bottom,
      aw.ful.layout.suit.tile.top,
      aw.ful.layout.suit.fair,
      aw.ful.layout.suit.fair.horizontal,
      aw.ful.layout.suit.spiral,
      aw.ful.layout.suit.spiral.dwindle,
      aw.ful.layout.suit.max,
      aw.ful.layout.suit.max.fullscreen,
      aw.ful.layout.suit.magnifier,
      aw.ful.layout.suit.corner.nw,
      -- awe.ful.layout.suit.corner.ne,
      -- awe.ful.layout.suit.corner.sw,
      -- awe.ful.layout.suit.corner.se,
  }
end}
