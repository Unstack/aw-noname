return {
  about = {
    blurb="Set of hotkeys for clients",
    description=[[
      Sets some.clientkeys to a predefined lua table of awful keys meant for clients (application windows)
    ]],
    requires={
      aw={"ful","gears"},
    },
    overrides={
      some={"clientkeys"}
    },
  },
  init=function(aw,some,config)
  some.clientkeys = aw.gears.table.join(
    aw.ful.key({ config.modkey, }, "f",
      function (c)
        c.fullscreen = not c.fullscreen
        c:raise()
      end,
    {description = "toggle fullscreen", group = "client"}),
    aw.ful.key({ config.modkey, "Shift" }, "c", function (c) c:kill() end,
    {description = "close", group = "client"}),
    aw.ful.key({ config.modkey, "Control" }, "space", aw.ful.client.floating.toggle,
    {description = "toggle floating", group = "client"}),
    aw.ful.key({ config.modkey, "Control" }, "Return", function (c) c:swap(aw.ful.client.getmaster()) end,
    {description = "move to master", group = "client"}),
    aw.ful.key({ config.modkey, }, "o", function (c) c:move_to_screen() end,
    {description = "move to screen", group = "client"}),
    aw.ful.key({ config.modkey, }, "t", function (c) c.ontop = not c.ontop end,
    {description = "toggle keep on top", group = "client"}),
    aw.ful.key({ config.modkey, }, "n",
      function (c)
        -- The client currently has the input focus, so it cannot be
        -- minimized, since minimized clients can't have the focus.
        c.minimized = true
      end,
    {description = "minimize", group = "client"}),
    aw.ful.key({ config.modkey, }, "m",
      function (c)
        c.maximized = not c.maximized
        c:raise()
      end,
    {description = "(un)maximize", group = "client"}),
    aw.ful.key({ config.modkey, "Control" }, "m",
      function (c)
        c.maximized_vertical = not c.maximized_vertical
        c:raise()
      end,
    {description = "(un)maximize vertically", group = "client"}),
    aw.ful.key({ config.modkey, "Shift" }, "m",
      function (c)
        c.maximized_horizontal = not c.maximized_horizontal
        c:raise()
      end,
    {description = "(un)maximize horizontally", group = "client"})
  )
end}
