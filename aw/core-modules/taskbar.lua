return {
about = {
  blurb="A taskbar wibar for awesome",
  description=[[
    Sets some.taskbar to a configurable taskbar factory.
  ]],
  requires={
    aw={"ful","gears","beautiful","wibox"},
    some={"sidemenu"} -- For launcher
  },
  overrides={
    some={"taskbar"}
  },
},
init = function(aw,some,config)
  some.taskbar = function(screen)
    local taskbar = aw.ful.wibar(aw.gears.table.join(
                              { screen = screen },
                              config.taskbar
                            ))

    local taglist_buttons = aw.gears.table.join(
                        aw.ful.button({ }, 1, function(t) t:view_only() end),
                        aw.ful.button({ config.modkey }, 1, function(t)
                                                  if client.focus then
                                                      client.focus:move_to_tag(t)
                                                  end
                                              end),
                        aw.ful.button({ }, 3, aw.ful.tag.viewtoggle),
                        aw.ful.button({ config.modkey }, 3, function(t)
                                                  if client.focus then
                                                      client.focus:toggle_tag(t)
                                                  end
                                              end),
                        aw.ful.button({ }, 4, function(t) aw.ful.tag.viewnext(t.screen) end),
                        aw.ful.button({ }, 5, function(t) aw.ful.tag.viewprev(t.screen) end)
                    )

    local tasklist_buttons = aw.gears.table.join(
                         aw.ful.button({ }, 1, function (c)
                                                  if c == client.focus then
                                                      c.minimized = true
                                                  else
                                                      -- Without this, the following
                                                      -- :isvisible() makes no sense
                                                      c.minimized = false
                                                      if not c:isvisible() and c.first_tag then
                                                          c.first_tag:view_only()
                                                      end
                                                      -- This will also un-minimize
                                                      -- the client, if needed
                                                      client.focus = c
                                                      c:raise()
                                                  end
                                              end),
                         aw.ful.button({ }, 3, aw.helpers.client_menu_toggle()),
                         aw.ful.button({ }, 4, function ()
                                                  aw.ful.client.focus.byidx(1)
                                              end),
                         aw.ful.button({ }, 5, function ()
                                                  aw.ful.client.focus.byidx(-1)
                                              end))

     -- Custom launcher for sidemenu
     screen.launcher = aw.ful.widget.launcher({ image = aw.beautiful.awesome_icon,
                                          menu=some.menu })

    -- Create a taglist widget
    screen.taglist = aw.ful.widget.taglist(screen, aw.ful.widget.taglist.filter.all, taglist_buttons)

    -- Create a tasklist widget
    screen.tasklist = aw.ful.widget.tasklist(screen, aw.ful.widget.tasklist.filter.currenttags, tasklist_buttons)

    -- Create an imagebox widget which will contain an icon indicating which layout we're using.
    -- We need one layoutbox per screen.
    screen.layoutbox = aw.ful.widget.layoutbox(screen)

    -- Assign button commands to layout box widget
    screen.layoutbox:buttons(aw.gears.table.join(
                           aw.ful.button({ }, 1, function () aw.ful.layout.inc( 1) end),
                           aw.ful.button({ }, 3, function () aw.ful.layout.inc(-1) end),
                           aw.ful.button({ }, 4, function () aw.ful.layout.inc( 1) end),
                           aw.ful.button({ }, 5, function () aw.ful.layout.inc(-1) end)
                         ))

    -- Add widgets to the taskbar
    taskbar:setup {
        layout = aw.wibox.layout.align.horizontal,
        { -- Left widgets
            layout = aw.wibox.layout.fixed.horizontal,
            screen.launcher,
            screen.taglist,
            aw.ful.widget.prompt()
        },
        screen.mytasklist, -- Middle widget
        { -- Right widgets
            layout = aw.wibox.layout.fixed.horizontal,
            aw.wibox.widget.systray(),
            aw.wibox.widget.textclock(),
            screen.layoutbox,
        },
    }
    return taskbar
  end
end
}
