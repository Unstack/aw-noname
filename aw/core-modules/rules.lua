return {
  about = {
    blurb="Predefined set of screen rules",
    description=[[
      Sets some.rules to a lua table of screen display rules
    ]],
    requires={
      aw={"ful","gears"},
    },
    overrides={
      some={"rules"}
    },
  },
  setup=function(aw,some,config)
    local clientkeys = some.clientkeys

    local clientbuttons = aw.gears.table.join(
        aw.ful.button({ }, 1, function (c) client.focus = c; c:raise() end),
        aw.ful.button({ config.modkey }, 1, aw.ful.mouse.client.move),
        aw.ful.button({ config.modkey }, 3, aw.ful.mouse.client.resize))

    some.rules = {
        -- All clients will match this rule.
        { rule = { },
          properties = {
                         border_width = aw.beautiful.border_width,
                         border_color = aw.beautiful.border_normal,
                         focus = aw.ful.client.focus.filter,
                         raise = true,
                         keys = clientkeys,
                         buttons = clientbuttons,
                         screen = aw.ful.screen.preferred,
                         placement = aw.ful.placement.no_overlap+aw.ful.placement.no_offscreen
         }
        },

        -- Floating clients.
        { rule_any = {
            instance = {
              "DTA",  -- Firefox addon DownThemAll.
              "copyq",  -- Includes session name in class.
            },
            class = {
              "Arandr",
              "Gpick",
              "Kruler",
              "MessageWin",  -- kalarm.
              "Sxiv",
              "Wpa_gui",
              "pinentry",
              "veromix",
              "xtightvncviewer"},

            name = {
              "Event Tester",  -- xev.
            },
            role = {
              "AlarmWindow",  -- Thunderbird's calendar.
              "pop-up",       -- e.g. Google Chrome's (detached) Developer Tools.
            }
          }, properties = { floating = true }},

        -- Add titlebars to normal clients and dialogs
        { rule_any = {type = { "normal", "dialog" }
          }, properties = { titlebars_enabled = true }
        },

        -- Set Firefox to always map on the tag named "2" on screen 1.
        -- { rule = { class = "Firefox" },
        --   properties = { screen = 1, tag = "2" } },
    }
end}
