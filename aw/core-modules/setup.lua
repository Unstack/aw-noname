return {about = {
  blurb="Desktop booter for awesome",
  description=[[
    Initializes beautiful and connects screens on aw::start.
  ]],
  requires={
    aw={"ful","gears","beautiful"},
    some={
      "mouse",         -- Mouse hotkeys
      "keys",          -- General hotkeys
      "defaultScreen", -- Screen to apply
      "rules"          -- Screen rules
    }
  },
  overrides={
    aw={"helpers"}
  },
},
  -- This will run at startup
  start=function(aw,some,config)
      -- Global mouse bindings
      root.buttons(some.mouse)

      -- Global key bindings
      root.keys(some.keys)

      -- Custom rules
      aw.ful.rules.rules = some.rules

      -- Apply default screen to each screen
      aw.ful.screen.connect_for_each_screen(some.defaultScreen)

      -- Set the terminal for applications that require it
      aw.menubar.utils.terminal = config.terminal
end
}
