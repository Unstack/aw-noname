return {
  about = {
    blurb="Basic error handler",
    description=[[
      Displays a notification at start if any startup errors were encountered.
      Will do the same if any other error happens afterwards.
    ]],
    requires={
      aw={"ful","gears"},
    },
    overrides={
      some={"keys"}
    },
  },
  init=function(aw)
    if awesome.startup_errors then
      aw.naughty.notify({ preset = aw.naughty.config.presets.critical,
                       title = "YOU DONE GOOFED UP NOW",
                       text = awesome.startup_errors,
                       position = "top_middle",
                       width = 200,
                       margin = 5})
  end

  -- Handle runtime errors after startup
  do
      local in_error = false
      awesome.connect_signal("debug::error", function (err)
          -- Make sure we don't go into an endless error loop
          if in_error then return end
          in_error = true

          aw.naughty.notify({ preset = aw.naughty.config.presets.critical,
                           title = "OW!",
                           text = tostring(err) })
          in_error = false
      end)
  end
end}
