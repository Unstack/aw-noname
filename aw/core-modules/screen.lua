return {
  about = {
    blurb="Default screen setup",
    description=[[
      Provides a default screen in some.defaultScreen.
      Comes with a helper to change the screen wallpaper.
    ]],
    requires={
      aw={"ful","gears"},
      some={"taskbar"}
    },
    overrides={
      some={"set_wallpaper","defaultScreen"}
    },
  },
  setup = function(aw, some, config)
      -- Generate tags from workspaces
      some.tags = aw.helpers.table_keys(config.workspaces)

      -- Re-set wallpaper when a screen's geometry changes (e.g. different resolution)
      screen.connect_signal("property::geometry", some.set_wallpaper)
  end,
  init = function(aw, some, config)
    -- Helper function for changing wallpapers
    some.set_wallpaper = function(s)
      local wallpaper = config.desktop.wallpaper
      aw.gears.wallpaper.maximized(wallpaper, s, true)
    end

    some.defaultScreen = function(screen)
      -- Set the wallpaper
      some.set_wallpaper(screen)

      -- Each screen having its own tag table is kind of silly
      aw.ful.tag(some.tags, screen, some.layouts)

      -- Create the taskbar
      screen.taskbar = some.taskbar(screen)

    end
  end
}
