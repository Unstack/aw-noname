return {about = {
  blurb="Signals for clients",
  description=[[
    Enables a bunch of functionality for clients (application windows).
  ]],
  requires={
    aw={"ful","gears","beautiful","wibox"},
  },
},init=
  function(aw)-- Widget and layout library
  local layout = aw.wibox.layout

  -- Enable sloppy focus, so that focus follows mouse.
  client.connect_signal("mouse::enter", function(c)
      if aw.ful.layout.get(c.screen) ~= aw.ful.layout.suit.magnifier
          and aw.ful.client.focus.filter(c) then
          client.focus = c
      end
  end)

  client.connect_signal("focus", function(c) c.border_color = aw.beautiful.border_focus end)
  client.connect_signal("unfocus", function(c) c.border_color = aw.beautiful.border_normal end)

  -- {{{ Signals
  -- Signal function to execute when a new client appears.
  client.connect_signal("manage", function (c)
      -- Set the windows at the slave,
      -- i.e. put it at the end of others instead of setting it master.
      -- if not awesome.startup then awful.client.setslave(c) end

      if awesome.startup and
        not c.size_hints.user_position
        and not c.size_hints.program_position then
          -- Prevent clients from being unreachable after screen count changes.
          aw.ful.placement.no_offscreen(c)
      end
  end)

  -- Add a titlebar if titlebars_enabled is set to true in the rules.
  client.connect_signal("request::titlebars", function(c)
      -- buttons for the titlebar
      local buttons = aw.gears.table.join(
          aw.ful.button({ }, 1, function()
              client.focus = c
              c:raise()
              aw.ful.mouse.client.move(c)
          end),
          aw.ful.button({ }, 3, function()
              client.focus = c
              c:raise()
              aw.ful.mouse.client.resize(c)
          end)
      )

      aw.ful.titlebar(c) : setup {
          { -- Left
              aw.ful.titlebar.widget.iconwidget(c),
              buttons = buttons,
              layout  = layout.fixed.horizontal
          },
          { -- Middle
              { -- Title
                  align  = "center",
                  widget = aw.ful.titlebar.widget.titlewidget(c)
              },
              buttons = buttons,
              layout  = layout.flex.horizontal
          },
          { -- Right
              aw.ful.titlebar.widget.floatingbutton (c),
              aw.ful.titlebar.widget.maximizedbutton(c),
              --awful.titlebar.widget.stickybutton   (c),
              --awful.titlebar.widget.ontopbutton    (c),
              aw.ful.titlebar.widget.closebutton    (c),
              layout = layout.fixed.horizontal()
          },
          layout = layout.align.horizontal
      }
  end)
end}
