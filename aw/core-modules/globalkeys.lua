return {
  about = {
    blurb="Set of keyboard hotkeys",
    description=[[
      Sets some.keys to a predefined lua table of awful keys
    ]],
    requires={
      aw={"ful","gears"},
    },
    overrides={
      some={"keys"}
    },
  },
  init=function(aw,some,config)
  -- Key bindings
  some.keys = aw.gears.table.join(
      aw.ful.key({ config.modkey,           }, "s",      aw.hotkeys.show_help,
                {description="show help", group="awesome"}),
      aw.ful.key({ config.modkey,           }, "Left",   aw.ful.tag.viewprev,
                {description = "view previous", group = "tag"}),
      aw.ful.key({ config.modkey,           }, "Right",  aw.ful.tag.viewnext,
                {description = "view next", group = "tag"}),
      aw.ful.key({ config.modkey,           }, "Escape", aw.ful.tag.history.restore,
                {description = "go back", group = "tag"}),

      aw.ful.key({ config.modkey,           }, "j",
          function ()
              aw.ful.client.focus.byidx( 1)
          end,
          {description = "focus next by index", group = "client"}
      ),
      aw.ful.key({ config.modkey,           }, "k",
          function ()
              aw.ful.client.focus.byidx(-1)
          end,
          {description = "focus previous by index", group = "client"}
      ),
      aw.ful.key({ config.modkey,           }, "w", function () some.menu:show() end,
                {description = "show main menu", group = "awesome"}),

      -- Layout manipulation
      aw.ful.key({ config.modkey, "Shift"   }, "j", function () aw.ful.client.swap.byidx(  1)    end,
                {description = "swap with next client by index", group = "client"}),
      aw.ful.key({ config.modkey, "Shift"   }, "k", function () aw.ful.client.swap.byidx( -1)    end,
                {description = "swap with previous client by index", group = "client"}),
      aw.ful.key({ config.modkey, "Control" }, "j", function () aw.ful.screen.focus_relative( 1) end,
                {description = "focus the next screen", group = "screen"}),
      aw.ful.key({ config.modkey, "Control" }, "k", function () aw.ful.screen.focus_relative(-1) end,
                {description = "focus the previous screen", group = "screen"}),
      aw.ful.key({ config.modkey,           }, "u", aw.ful.client.urgent.jumpto,
                {description = "jump to urgent client", group = "client"}),
      aw.ful.key({ config.modkey,           }, "Tab",
          function ()
              aw.ful.client.focus.history.previous()
              if client.focus then
                  client.focus:raise()
              end
          end,
          {description = "go back", group = "client"}),

      -- Standard program
      aw.ful.key({ config.modkey,           }, "Return", function () aw.ful.spawn(config.terminal) end,
                {description = "open a terminal", group = "launcher"}),
      aw.ful.key({ config.modkey, "Control" }, "r", awesome.restart,
                {description = "reload awesome", group = "awesome"}),
      aw.ful.key({ config.modkey, "Shift"   }, "q", awesome.quit,
                {description = "quit awesome", group = "awesome"}),

      aw.ful.key({ config.modkey,           }, "l",     function () aw.ful.tag.incmwfact( 0.05)          end,
                {description = "increase master width factor", group = "layout"}),
      aw.ful.key({ config.modkey,           }, "h",     function () aw.ful.tag.incmwfact(-0.05)          end,
                {description = "decrease master width factor", group = "layout"}),
      aw.ful.key({ config.modkey, "Shift"   }, "h",     function () aw.ful.tag.incnmaster( 1, nil, true) end,
                {description = "increase the number of master clients", group = "layout"}),
      aw.ful.key({ config.modkey, "Shift"   }, "l",     function () aw.ful.tag.incnmaster(-1, nil, true) end,
                {description = "decrease the number of master clients", group = "layout"}),
      aw.ful.key({ config.modkey, "Control" }, "h",     function () aw.ful.tag.incncol( 1, nil, true)    end,
                {description = "increase the number of columns", group = "layout"}),
      aw.ful.key({ config.modkey, "Control" }, "l",     function () aw.ful.tag.incncol(-1, nil, true)    end,
                {description = "decrease the number of columns", group = "layout"}),
      aw.ful.key({ config.modkey,           }, "space", function () aw.ful.layout.inc( 1)                end,
                {description = "select next", group = "layout"}),
      aw.ful.key({ config.modkey, "Shift"   }, "space", function () aw.ful.layout.inc(-1)                end,
                {description = "select previous", group = "layout"}),

      aw.ful.key({ config.modkey, "Control" }, "n",
                function ()
                    local c = aw.ful.client.restore()
                    -- Focus restored client
                    if c then
                        client.focus = c
                        c:raise()
                    end
                end,
                {description = "restore minimized", group = "client"}),

      -- Prompt
      aw.ful.key({ config.modkey },            "r",     function () aw.ful.screen.focused().mypromptbox:run() end,
                {description = "run prompt", group = "launcher"}),

      aw.ful.key({ config.modkey }, "x",
                function ()
                    aw.ful.prompt.run {
                      prompt       = "Run Lua code: ",
                      textbox      = aw.ful.screen.focused().mypromptbox.widget,
                      exe_callback = aw.ful.util.eval,
                      history_path = aw.ful.util.get_cache_dir() .. "/history_eval"
                    }
                end,
                {description = "lua execute prompt", group = "awesome"}),
      -- Menubar
      aw.ful.key({ config.modkey }, "p", function() aw.menubar.show() end,
                {description = "show the menubar", group = "launcher"})
  )

  -- Bind all key numbers to tags.
  -- Be careful: we use keycodes to make it work on any keyboard layout.
  -- This should map on the top row of your keyboard, usually 1 to 9.
  for i = 1, 9 do
      some.keys = aw.gears.table.join(some.keys,
          -- View tag only.
          aw.ful.key({ config.modkey }, "#" .. i + 9,
                    function ()
                          local screen = aw.ful.screen.focused()
                          local tag = screen.tags[i]
                          if tag then
                             tag:view_only()
                          end
                    end,
                    {description = "view tag #"..i, group = "tag"}),
          -- Toggle tag display.
          aw.ful.key({ config.modkey, "Control" }, "#" .. i + 9,
                    function ()
                        local screen = aw.ful.screen.focused()
                        local tag = screen.tags[i]
                        if tag then
                           aw.ful.tag.viewtoggle(tag)
                        end
                    end,
                    {description = "toggle tag #" .. i, group = "tag"}),
          -- Move client to tag.
          aw.ful.key({ config.modkey, "Shift" }, "#" .. i + 9,
                    function ()
                        if client.focus then
                            local tag = client.focus.screen.tags[i]
                            if tag then
                                client.focus:move_to_tag(tag)
                            end
                       end
                    end,
                    {description = "move focused client to tag #"..i, group = "tag"}),
          -- Toggle tag on focused client.
          aw.ful.key({ config.modkey, "Control", "Shift" }, "#" .. i + 9,
                    function ()
                        if client.focus then
                            local tag = client.focus.screen.tags[i]
                            if tag then
                                client.focus:toggle_tag(tag)
                            end
                        end
                    end,
                    {description = "toggle focused client on tag #" .. i, group = "tag"})
      )
    end
  end}
