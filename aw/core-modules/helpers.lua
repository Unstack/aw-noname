return {about = {
  blurb = "Helpers for aw",
  description = [[
    Sets aw.helpers to a table of helper functions.
    Included are: table_keys, table_reverse and client_menu_toggle
  ]],
  requires = {
    aw = {"ful", "gears"},
  },
  overrides = {
    aw = {"helpers"}
  },
}, init = function(aw)
  aw.helpers = {}

  aw.helpers.client_menu_toggle = function()
    local instance = nil

    return function ()
      if instance and instance.wibox.visible then
        instance:hide()
        instance = nil
      else
        instance = aw.ful.menu.clients({ theme = { width = 250 } })
      end
    end
  end

  aw.helpers.table_keys = function(_table)
    local a = {}
    for n in pairs(_table) do table.insert(a, n) end
    return a
  end

  aw.helpers.table_reverse = function(arr)
    local i, j = 1, #arr

    while i < j do
      arr[i], arr[j] = arr[j], arr[i]

      i = i + 1
      j = j - 1
    end
  end
end}
