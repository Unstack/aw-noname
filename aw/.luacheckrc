std = "luajit"

unused_args=false

-- these globals can be set and accessed.
globals = {"awesome","client","screen","root"}
read_globals = {} -- these globals can only be accessed.
