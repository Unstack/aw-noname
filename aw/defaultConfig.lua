return {
  version={0,2,5},
  moduleDirectories={
    "core-modules",
    "../modules"
  },
  debug=false,
  blacklist={},
  whitelist=nil,
  terminal = "xfce4-terminal", -- This is used later as the default terminal and editor to run
  editor = "mousepad",
  editor_cmd = "mousepad",
  -- Default modkey.
  -- Usually, Mod4 is the key with a logoLastE between Control and Alt.
  -- If you do not like this or do not have such a key,
  -- I suggest you to remap Mod4 to another key using xmodmap or other tools.
  -- However, you can use another modifier like Mod1, but it may interact with others.
  modkey = "Mod4",
  theme = "zenburn/theme.lua", -- Global theme
  font = "Ubuntu 12",          -- Global Font
  taskbar = {
    position = "bottom",
    height = 30,
    content = {}
  },
  workspaces = {1,2,3,4,5,6,7,8,9},
  desktop = {
    --Fallback wallpaper (only displayed if workspace doesn't have one)
    wallpaper="/home/flubber/pictures/wallpapers/nature/5 - TTGfzly.jpg"
  }
}
