local os = require("os")

-- Microlibrary for awesome
local gears = require("gears")
local aw = gears.object{}

-- awesome dependencies
aw.gears          = gears
aw.naughty        = require("naughty")
aw.ful            = require("awful")
aw.beautiful      = require("beautiful")
aw.wibox          = require("wibox")
aw.menubar        = require("menubar")

aw.hotkeys        = require("awful.hotkeys_popup").widget

-- Enable hotkeys help widget for VIM and other apps when client with a
-- matching name is opened:
require("awful.hotkeys_popup.keys")

aw.autofocus      = require("awful.autofocus")

-- Fallback configuration
aw.defaultConfig  = require("./aw/defaultConfig")

-- Environment information
aw.pwd            = io.popen"pwd":read'*l'
aw.prefix         = "/aw/"
aw.path           = aw.pwd..aw.prefix
aw.terminalwidth  = 80
aw.halfline       = string.rep("~",aw.terminalwidth/2)


aw.STATES = {
  ERROR=-1,
  INITIAL=0,
  READY=1,
  STARTED=2,
  EXIT=3
}

aw.state = aw.STATES.INITIAL

-- Lua implementation of PHP scandir function
-- https://stackoverflow.com/questions/5303174/how-to-get-list-of-directories-in-lua
aw.scandir = function(directory)
    local i, t, popen = 0, {}, io.popen
    local pfile = popen('ls -a "'..directory..'"')
    for filename in pfile:lines() do
        i = i + 1
        t[i] = filename
    end
    pfile:close()
    return t
end

aw.filter = function(list,el)
    return aw.gears.table.hasitem(list,el)==nil
end

aw.loadModules = function(paths,blacklist,whitelist)
  local out = {}
  for k,path in pairs(paths) do
    local modules = aw.scandir(aw.path..path)
    aw.print("Loading modules from:",path)
    aw.print("Blacklist entries:",blacklist and #blacklist or "none")
    aw.print("Whitelist entries:",whitelist and #whitelist or "none")
    for x,filepath in pairs(modules) do
      local filetype = string.find(filepath,'.lua')
      local module, prettyname, about, spacer

      if type(filetype) == "number" then
          prettyname = string.sub(filepath,0,filetype-1)
          if (whitelist == nil or not aw.filter(whitelist,prettyname)) and
             (blacklist == nil or aw.filter(blacklist,prettyname)) then
            module = aw.try(function() return dofile(aw.path..path.."/"..filepath) end)
          end
      end

      if module ~= nil then
        out[prettyname] = module
        about = module.about and module.about.blurb or "[nondescript]"
        spacer = string.rep(" ",aw.terminalwidth-#prettyname-#about)
        aw.print("","⤷ Loaded module:",prettyname..spacer..about)
      elseif filepath ~= "." and filepath ~= ".." then
        aw.print("\t⤷ Skipping file:",filepath)
      end
    end
  end
  return out
end

aw.try = function(func,...)
  local out,onfail,onsuccess

  if type(func)=="function" then
    out = aw.gears.protected_call(func,...)
  elseif type(func)=="table" then
    out = aw.gears.protected_call(func.call,...)
    onfail = func.onfail
    onsuccess = func.onsuccess
  end

  if out == nil then
    if onfail then
        onfail()
    end
  elseif onsuccess ~= nil then
    onsuccess()
  end

  return out
end

aw.bench = function(...)
  local time = os.clock()
  local out = aw.try(...)
  local bench = os.clock() - time
  return out,bench
end

aw.mark = function(previous)
  if previous then
    return os.clock() - previous
  else
    return os.clock()
  end
end

aw.conditionalPrinter = function(table,condition)
  return function(...)
    if table[condition] then
      print(...)
    end
  end
end

aw.initialize = function(config)
  if aw.state == aw.STATES.STARTED then
    return error("aw error: aw already started")
  end

  if aw.state == aw.STATES.READY then
    return error("aw error: aw already initialized")
  end

  if aw.some then
    print("aw warning: Pre-exisiting container found")
  end

  aw.some = {}

  aw.print = aw.conditionalPrinter(config,"debug")

  if aw.beautiful then
    aw.beautiful.init(aw.gears.filesystem.get_themes_dir() .. config.theme)
  end

  aw.print("\n~~~~~~~~~~~~")
  aw.print("/ _` \\ \\/\\/ /")
  aw.print("\\__,_|\\_/\\_/")
  aw.print("~~~["..table.concat(aw.defaultConfig.version,".").."]~~~")

  local benchmark = {disk=0,aw=0}
  local count = 0
  local _

  aw.modules, benchmark['disk'] = aw.bench( aw.loadModules,
                                            config.moduleDirectories,
                                            config.blacklist,
                                            config.whitelist )

  _,benchmark['aw'] = aw.bench(function()
    for name,package in pairs(aw.modules) do
      if type(package)=="table" then
        count=count+1
        if type(package.init) == "function" then
          aw.try( function() package.init(aw,	aw.some,	config) end)
        end
        if type(package.setup) ~= "nil" then
              if type(package.setup) == "function" then
                    aw:connect_signal("aw::setup", package.setup)
              else
                if type(package.setup.pre) == "function" then
                      aw:connect_signal("aw::setup::pre", package.setup.pre)
                end
                if type(package.setup.post) == "function" then
                      aw:connect_signal("aw::setup::post", package.setup.post)
                end
              end
        end
        if type(package.start) ~= "nil" then
              if type(package.start) == "function" then
                    aw:connect_signal("aw::start", package.start)
              else
                if type(package.start.pre) == "function" then
                      aw:connect_signal("aw::start::pre", package.start.pre)
                end
                if type(package.start.post) == "function" then
                      aw:connect_signal("aw::start::post", package.start.post)
                end
              end
        end
     end
   end
end)

  aw.print("Loaded "..count.." modules from disk in "..math.floor(benchmark.disk*1000).."ms")
  aw.print("Loading modules into aw took "..math.floor(benchmark.aw*1000).."ms")

  aw.state = aw.STATES.READY

  return aw.some
end

aw.start = function(container,config)
  if aw.state == aw.STATES.STARTED then
    return error("aw error: aw already started")
  end

  aw.print("~~~[aw::start]~~~")

  if aw.try ( function()
                aw:emit_signal("aw::start::pre",  container, config)
                aw:emit_signal("aw::start",       container, config)
                aw:emit_signal("aw::start::post", container, config)
              end) then
    aw.state = aw.STATES.STARTED
  else
    aw.state = aw.STATES.ERROR
  end
end

aw.setup = function(userconfig)
  local config = aw.gears.table.join({}, aw.defaultConfig, userconfig)
  local container = aw.initialize( config )

  aw.print("~~~[aw::setup]~~~")
  if aw.try ( function()
                aw:emit_signal("aw::setup::pre",   container, config)
                aw:emit_signal("aw::setup",        container, config)
                aw:emit_signal("aw::setup::post",  container, config)
              end) then
    aw.state = aw.STATES.READY
  else
    aw.state = aw.STATES.ERROR
  end

  aw.print("setup finished")

  if not userconfig.noautostart then
    aw.start( container, config )
  end

  aw.print("start finished")

  return aw,container,config
end

return aw.setup
