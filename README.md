# awm-noname

Awesome window management scripts for Awesome WM.

![A preview image of awm-noname](https://imgur.com/MZj50Pll.png)

## Goals
1. A clean desktop environment tailored towards power users
2. A framework on top of Awesome designed for full customization
3. A collection of extensions to make Awesome even more Awesome

## Activity
- Working on:
    - Awe micro-framework:
        - Module blacklisting and overloading
        - Collision checking between modules
        - Hotloading / restarting of modules
        - Improved error handling and logging
- In consideration:
    - Sessions
        - Sessions storing, loading and clearing
- Planned:
    - Workspaces
        - Make tags useful
        - Integrate with Awesome rules
    - Taskbar widget
    - TMUX integration
    - Additional panels
        - Lemonbar / dzen2 / dmenu support
    - GUI config editor
- Finished:
  - Awe micro-framework:
    - Break the original monolithic rc into modules
    - Keep data and logic as seperated as possible
    - Store every configurable property in a monolithic config
    - Split said config into seperate modules again
    - Seperate front (configs) and back (rc logic) from eachother
    - Events, event handling and main event loop
    - Awe-level error handling and logging

## Revision history
* 0.2.5
    - *12-9-18*
        - Implemented subevents aw.setup.pre and aw.setup.post which are ran before and after aw.start respectively. Same for start.
        - Moved event system from container to core aw framework for better support with awesome's signal system first and semantics second
        - Cleanup in some modules, minor bugfixes.
* 0.2.0
    - *31-8-18*
        - Renamed awe to aw
        - Implemented events 'setup' and 'start'
        - Minor error handler implemented
        - Modules overhauled to be awlike
        - Cleanup and bugfixes across several modules
        - Removed a few obsolete modules

* 1.1.0 [Awe]
    - *31-8-18*
        - Implemented awe, a micro-framework for AwesomeWM
        - Full backwards-compatibility with Awesome
        - Awesome scripts may be loaded by placing them in `modules`, any calls to awe will be handled appropriately

* 1.0.0
    - *29-8-18*
        - Split original Awesome RC into more manageable modules
        - Minor changes for improved scalability and configurability
        - Default theme set to zenburn
